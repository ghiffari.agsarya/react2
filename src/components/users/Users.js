import React, { Component } from "react";
import { Card, CardBody, Col, Container, Row, CardGroup } from "reactstrap";
import "../../App.css";

export default class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            isLoaded: false,
        };
    }

    componentDidMount() {
        fetch(`http://localhost:8084/customer/account`, {
            method: "POST",
            headers: {
                "content-type": "application/json",
                Authorization:
                    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X251bWJlciI6ODMyNzEyLCJlbWFpbCI6IiJ9.jn93A9VuY9jhAE7U3GsY3m45UUNcKXkTOR3K9vQzNvk",
            },
        })
            .then((res) => res.json())
            .then((json) => {
                this.setState({
                    isLoaded: true,
                    users: json,
                });
            })
            .catch((error) => {
                this.setState({
                    error: error.message,
                    isLoaded: false,
                });
            });
    }

    renderUsers = () => {
        const { isLoaded, users } = this.state;

        if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="app flex-row align-items-center">
                    <Container>
                        <Row className="justify-content-center">
                            <Col md="9" lg="7" xl="6">
                                <CardGroup>
                                    <Card className="p-2">
                                        <CardBody>
                                            <div>
                                                <h1>{JSON.stringify(users)}</h1>
                                                {/* {users.map((user) => (
                        <div key={user.id}>
                            Name: {user.name} | Email: {user.email}
                        </div>
                    ))}
                    ; */}
                                            </div>
                                        </CardBody>
                                    </Card>
                                </CardGroup>
                            </Col>
                        </Row>
                    </Container>
                </div>
            );
        }
    };

    render() {
        return <div>{this.renderUsers()}</div>;
    }
}
