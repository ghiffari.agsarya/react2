import React, { Component } from "react";
import {
    Button,
    Card,
    CardBody,
    Col,
    Container,
    Form,
    Input,
    InputGroup,
    Row,
} from "reactstrap";

class Register extends Component {
    constructor() {
        super();
        this.state = {
            name: "",
            email: "",
            password: "",
        };
        this.name = this.name.bind(this);
        this.email = this.email.bind(this);
        this.password = this.password.bind(this);
        this.register = this.register.bind(this);
    }

    email(event) {
        this.setState({ email: event.target.value });
    }

    password(event) {
        this.setState({ password: event.target.value });
    }

    name(event) {
        this.setState({ name: event.target.value });
    }

    register(event) {
        event.preventDefault();

        fetch("http://54.198.99.77:8090/api/v1/account/add", {
            method: "post",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                name: this.state.name,
                password: this.state.password,
                email: this.state.email,
            }),
        })
            .then((Response) => Response.json())
            .then((Result) => {
                console.log(Result);
                // if (Result.Status === "Success")
                this.props.history.push("/Dashboard");
                // else alert("Sorrrrrry !!!! Un-authenticated User !!!!!");
            });
    }

    render() {
        return (
            <div className="app flex-row align-items-center">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="9" lg="7" xl="6">
                            <Card className="mx-4">
                                <CardBody className="p-4">
                                    <Form>
                                        <div
                                            class="row"
                                            className="mb-2 pageheading"
                                        >
                                            <div class="col-sm-12 btn btn-primary">
                                                Sign Up
                                            </div>
                                        </div>

                                        <InputGroup className="mb-3">
                                            <Input
                                                type="text"
                                                onChange={this.name}
                                                placeholder="Masukkan Nama"
                                            />
                                        </InputGroup>

                                        <InputGroup className="mb-3">
                                            <Input
                                                type="text"
                                                onChange={this.email}
                                                placeholder="Masukkan Email"
                                            />
                                        </InputGroup>

                                        <InputGroup className="mb-3">
                                            <Input
                                                type="password"
                                                onChange={this.password}
                                                placeholder="Masukkan Password"
                                            />
                                        </InputGroup>

                                        <Button
                                            onClick={this.register}
                                            color="success"
                                            block
                                        >
                                            Create Account
                                        </Button>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Register;
