import React, { Component } from "react";
import "../../App.css";
import {
    Button,
    Card,
    CardBody,
    CardGroup,
    Col,
    Container,
    Form,
    Input,
    InputGroup,
    Row,
} from "reactstrap";

class Deposit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Amount: "",
        };
        this.Amount = this.Amount.bind(this);
        this.deposit = this.deposit.bind(this);
    }

    Amount(event) {
        this.setState({ Amount: event.target.value });
    }

    deposit(event) {
        event.preventDefault();

        fetch("http://localhost:8084/customer/deposit", {
            method: "post",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization:
                    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X251bWJlciI6ODMyNzEyLCJlbWFpbCI6IiJ9.jn93A9VuY9jhAE7U3GsY3m45UUNcKXkTOR3K9vQzNvk",
            },
            body: JSON.stringify({
                Amount: parseInt(this.state.Amount),
            }),
        })
            .then((Response) => Response.json())
            .then((result) => {
                console.log(result);
                // if (result.status == "Invalid") alert("Invalid User");
                // else
                this.props.history.push("/Dashboard");
            });
    }

    render() {
        return (
            <div className="app flex-row align-items-center">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="9" lg="7" xl="6">
                            <CardGroup>
                                <Card className="p-2">
                                    <CardBody>
                                        <Form>
                                            <div
                                                class="row"
                                                className="mb-2 pageheading"
                                            >
                                                <div class="col-sm-12 btn btn-primary">
                                                    Deposit
                                                </div>
                                            </div>

                                            <InputGroup className="mb-4">
                                                <Input
                                                    type="number"
                                                    onChange={this.Amount}
                                                    placeholder="Masukkan jumlah"
                                                />
                                            </InputGroup>

                                            <Button
                                                onClick={this.deposit}
                                                color="success"
                                                block
                                            >
                                                Deposit
                                            </Button>
                                        </Form>
                                    </CardBody>
                                </Card>
                            </CardGroup>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Deposit;
