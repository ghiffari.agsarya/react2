import React from "react";
import Login from "./components/login/Login";
import Register from "./components/register/Register";
import Dashboard from "./components/dashboard/Dashboard";
import User from "./components/users/Users";
import Withdraw from "./components/Transaksi/Withdraw";
import Transfer from "./components/Transaksi/Transfer";
import Deposit from "./components/Transaksi/Deposit";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function App() {
    return (
        <Router>
            <div className="container">
                <nav className="navbar navbar-expand-lg navheader">
                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link to={"/Login"} className="nav-link">
                                    Login
                                </Link>
                            </li>

                            <li className="nav-item">
                                <Link to={"/Signup"} className="nav-link">
                                    Sign Up
                                </Link>
                            </li>

                            <li className="nav-item">
                                <Link to={"/User"} className="nav-link">
                                    User
                                </Link>
                            </li>

                            <li className="nav-item">
                                <Link to={"/Withdraw"} className="nav-link">
                                    Withdraw
                                </Link>
                            </li>

                            <li className="nav-item">
                                <Link to={"/Transfer"} className="nav-link">
                                    Transfer
                                </Link>
                            </li>

                            <li className="nav-item">
                                <Link to={"/Deposit"} className="nav-link">
                                    Deposit
                                </Link>
                            </li>
                        </ul>
                    </div>
                </nav>
                <br />
                <Switch>
                    <Route exact path="/Login" component={Login} />
                    <Route path="/Signup" component={Register} />
                    <Route path="/User" component={User} />
                    <Route path="/Withdraw" component={Withdraw} />
                    <Route path="/Transfer" component={Transfer} />
                    <Route path="/Deposit" component={Deposit} />
                </Switch>
                <Switch>
                    <Route path="/Dashboard" component={Dashboard} />
                </Switch>
            </div>
        </Router>
    );
}

export default App;
